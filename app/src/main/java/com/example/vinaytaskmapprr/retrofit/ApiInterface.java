package com.example.vinaytaskmapprr.retrofit;

import com.example.vinaytaskmapprr.model.Contributors;
import com.example.vinaytaskmapprr.model.Repositories;
import com.example.vinaytaskmapprr.model.Repository;
import com.example.vinaytaskmapprr.model.RepositoryOwner;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface ApiInterface {

    @GET("search/repositories")
    Call<Repositories> getRepository(@QueryMap Map<String, String> searchOptions);

    @GET("repos/{full_name}")
    Call<Repository> getUser(@Path(value = "full_name", encoded = true) String fullName);

    @GET("repos/{full_name}/contributors")
    Call<List<Contributors>> get(@Path(value = "full_name", encoded = true) String fullName);

    @GET
    Call<List<Repository>> getRepositories(@Url String url);

    @GET
    Call<RepositoryOwner> getContributorDetails(@Url String url);
}
