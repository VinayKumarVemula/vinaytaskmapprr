package com.example.vinaytaskmapprr.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Repositories implements Serializable {

    @SerializedName("items")
    private ArrayList<Repository> repositoryArrayList;

    public ArrayList<Repository> getRepositoryArrayList() {
        return repositoryArrayList;
    }

    public void setRepositoryArrayList(ArrayList<Repository> repositoryArrayList) {
        this.repositoryArrayList = repositoryArrayList;
    }
}
