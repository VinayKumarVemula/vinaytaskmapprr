package com.example.vinaytaskmapprr.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Repository implements Serializable {

    @SerializedName("name")
    private String name = "";

    @SerializedName("full_name")
    private String full_name = "";

    @SerializedName("description")
    private String description = "";

    @SerializedName("html_url")
    private String html_url = "";

    @SerializedName("contributors_url")
    private String contributors_url = "";

    @SerializedName("stargazers_count")
    private int stargazers_count = 0;

    @SerializedName("forks_count")
    private int forks_count = 0;

    @SerializedName("owner")
    private RepositoryOwner owner;

    @SerializedName("watchers_count")
    private int watchers_count = 0;

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getContributors_url() {
        return contributors_url;
    }

    public void setContributors_url(String contributors_url) {
        this.contributors_url = contributors_url;
    }

    public int getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(int stargazers_count) {
        this.stargazers_count = stargazers_count;
    }


    public RepositoryOwner getOwner() {
        return owner;
    }

    public void setOwner(RepositoryOwner owner) {
        this.owner = owner;
    }

    public int getWatchers_count() {
        return watchers_count;
    }

    public void setWatchers_count(int watchers_count) {
        this.watchers_count = watchers_count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
