package com.example.vinaytaskmapprr.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.vinaytaskmapprr.R;
import com.example.vinaytaskmapprr.adapter.ContributorAdapter;
import com.example.vinaytaskmapprr.model.Contributors;
import com.example.vinaytaskmapprr.model.Repository;
import com.example.vinaytaskmapprr.retrofit.ApiInterface;
import com.example.vinaytaskmapprr.retrofit.AppClient;
import com.example.vinaytaskmapprr.utility.Util;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepoDetailsActivity extends AppCompatActivity {

    private ImageView repoImage;
    private TextView tvRepoName, repoFullName, repoDescription, repoUrl;
    private RecyclerView recyclerView;
    private ContributorAdapter contributorAdapter;
    private Repository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        repoFullName = findViewById(R.id.tv_user_name);
        tvRepoName = findViewById(R.id.tv_repo_name);
        repoUrl = findViewById(R.id.tv_repo_url);
        repoDescription = findViewById(R.id.tv_description);
        repoImage = findViewById(R.id.user_image);
        recyclerView = findViewById(R.id.contributor_recylcerview);
        ImageView backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        repoUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WebViewRepositoryActivity.class);
                intent.putExtra("html_url", repository.getHtml_url());
                startActivity(intent);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        Intent intent = getIntent();
        String fullname = intent.getStringExtra("full_name");

        if (Util.isNetWorkConnected(getApplicationContext())) {
            fetchRepoInfo(fullname);
            fetchContributors(fullname);
        } else {
            Toast.makeText(getApplicationContext(), "Internet not available", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void fetchRepoInfo(String fullname) {
        ApiInterface api = AppClient.getRetrofit(getApplicationContext()).create(ApiInterface.class);
        Call<Repository> call = api.getUser(fullname);

        call.enqueue(new Callback<Repository>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<Repository> call, Response<Repository> response) {
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        repository = response.body();
                        tvRepoName.setText(repository.getName() + "");
                        repoFullName.setText(repository.getFull_name() + "");
                        repoUrl.setText(repository.getHtml_url() + "");
                        repoDescription.setText(repository.getDescription() + "");
                        Glide.with(getApplicationContext())
                                .load(repository.getOwner().getAvatar_url())
                                .placeholder(R.mipmap.ic_launcher)
                                .centerCrop()
                                .into(repoImage);
                    } else
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Repository> call, Throwable t) {
                System.out.println("failed url " + call.toString());
            }
        });
    }

    public void fetchContributors(String name) {
        ApiInterface api = AppClient.getRetrofit(getApplicationContext()).create(ApiInterface.class);
        Call<List<Contributors>> call = api.get(name);

        call.enqueue(new Callback<List<Contributors>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<List<Contributors>> call, Response<List<Contributors>> response) {
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        List<Contributors> contributorsList = response.body();
                        contributorAdapter = new ContributorAdapter(getApplicationContext(), contributorsList);
                        recyclerView.setAdapter(contributorAdapter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Contributors>> call, Throwable t) {
                System.out.println("failed url " + call.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
