package com.example.vinaytaskmapprr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.vinaytaskmapprr.R;


public class WebViewRepositoryActivity extends AppCompatActivity {

    private ProgressBar progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_repository);

        ImageView backButton = findViewById(R.id.arw_cross);
        TextView tvHtmlUrl = findViewById(R.id.arw_repo_url);
        WebView webView = findViewById(R.id.arw_webview);
        progressDialog = findViewById(R.id.arw_progress_dialog);

        Intent i = getIntent();
        String htmlUrl = i.getStringExtra("html_url");

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(htmlUrl);

        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                progressDialog.setVisibility(View.GONE);
            }
        });

        tvHtmlUrl.setText(htmlUrl);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

}
