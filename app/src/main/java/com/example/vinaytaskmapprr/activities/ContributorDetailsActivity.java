
package com.example.vinaytaskmapprr.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.vinaytaskmapprr.R;
import com.example.vinaytaskmapprr.adapter.RepositoryAdapter;
import com.example.vinaytaskmapprr.model.Repository;
import com.example.vinaytaskmapprr.model.RepositoryOwner;
import com.example.vinaytaskmapprr.retrofit.ApiInterface;
import com.example.vinaytaskmapprr.retrofit.AppClient;
import com.example.vinaytaskmapprr.utility.Util;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContributorDetailsActivity extends AppCompatActivity implements RepositoryAdapter.RepoItemClick {

    private TextView ownerName;
    private ImageView ownerImage;
    private RecyclerView recyclerView;
    private RepositoryAdapter repoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributor_details);
        ownerName = findViewById(R.id.tv_user_name);
        ownerImage = findViewById(R.id.user_image);
        recyclerView = findViewById(R.id.repos_recyclerview);
        ImageView backButton = findViewById(R.id.image_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent i = getIntent();
        String url = i.getStringExtra("repos");
        String repoUrl = i.getStringExtra("repo_url");

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recyclerView.setItemAnimator(itemAnimator);
        recyclerView.setNestedScrollingEnabled(false);

        if (Util.isNetWorkConnected(getApplicationContext())) {
            getContributorDetails(repoUrl);
            fetchRepos(url);
        } else {
            Toast.makeText(getApplicationContext(), "Internet not available", Toast.LENGTH_LONG).show();
        }
    }


    public void getContributorDetails(String url) {
        ApiInterface api = AppClient.getRetrofit(getApplicationContext()).create(ApiInterface.class);
        Call<RepositoryOwner> call = api.getContributorDetails(url);

        call.enqueue(new Callback<RepositoryOwner>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<RepositoryOwner> call, Response<RepositoryOwner> response) {
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        RepositoryOwner owner = response.body();
                        if (!TextUtils.isEmpty(owner.getName())) {
                            ownerName.setText("" + owner.getName());
                        } else {
                            ownerName.setText("User Details");
                        }
                        Glide.with(getApplicationContext()).load(owner.getAvatar_url())
                                .placeholder(R.mipmap.ic_launcher)
                                .centerCrop()
                                .into(ownerImage);
                    } else
                        Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RepositoryOwner> call, Throwable t) {
                System.out.println("failed url " + call.request().url());
                System.out.println("failed url error " + t.getMessage());
            }
        });
    }

    public void fetchRepos(String repoUrl) {
        ApiInterface api = AppClient.getRetrofit(getApplicationContext()).create(ApiInterface.class);
        Call<List<Repository>> call = api.getRepositories(repoUrl);

        call.enqueue(new Callback<List<Repository>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<List<Repository>> call, Response<List<Repository>> response) {
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        List<Repository> repositories = response.body();
                        if (repositories.size() > 0) {
                            repoAdapter = new RepositoryAdapter(getApplicationContext(), repositories, ContributorDetailsActivity.this);
                            recyclerView.setAdapter(repoAdapter);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Repository>> call, Throwable t) {
                System.out.println("failed url " + call.request().url());
                System.out.println("failed url error " + t.getMessage());
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void OnRepoItemClick(String fullName) {
        Intent intent = new Intent(getApplicationContext(), RepoDetailsActivity.class);
        intent.putExtra("full_name", fullName);
        ////intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
