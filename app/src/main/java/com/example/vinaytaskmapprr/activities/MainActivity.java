package com.example.vinaytaskmapprr.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vinaytaskmapprr.R;
import com.example.vinaytaskmapprr.adapter.RepositoryAdapter;
import com.example.vinaytaskmapprr.databinding.ActivityMainBinding;
import com.example.vinaytaskmapprr.model.Repositories;
import com.example.vinaytaskmapprr.model.Repository;
import com.example.vinaytaskmapprr.retrofit.ApiInterface;
import com.example.vinaytaskmapprr.retrofit.AppClient;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements RepositoryAdapter.RepoItemClick {

    ActivityMainBinding activityMainBinding;

    RepositoryAdapter repositoryAdapter;

    Calendar myCalendar = Calendar.getInstance();

    private String startDateString = "", endDateString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        activityMainBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        activityMainBinding.recyclerView.setItemAnimator(itemAnimator);
        activityMainBinding.recyclerView.setNestedScrollingEnabled(false);

        activityMainBinding.search.setActivated(true);
        activityMainBinding.search.setQueryHint("Search");
        activityMainBinding.search.onActionViewExpanded();
        activityMainBinding.search.setIconified(false);


        activityMainBinding.cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityMainBinding.search.setQuery("", false);
                activityMainBinding.search.clearFocus();

                activityMainBinding.recyclerView.setVisibility(View.GONE);
                activityMainBinding.emptyText.setVisibility(View.VISIBLE);
            }
        });

        activityMainBinding.imageFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filteringOptionsDialog();
            }
        });

        activityMainBinding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                Map<String, String> map = new HashMap<>();
                String result = getFilterByDate();
                String finalQuery = query + result;
                map.put("q", finalQuery);
                map.put("order", "desc");
                map.put("sort", "stars");

                activityMainBinding.search.clearFocus();

                getRepositories(map);
                activityMainBinding.recyclerView.setVisibility(View.VISIBLE);
                activityMainBinding.emptyText.setVisibility(View.GONE);
                return true;
            }
        });

        activityMainBinding.search.clearFocus();
    }


    private void getRepositories(Map<String, String> map) {
        ApiInterface api = AppClient.getRetrofit(getApplicationContext()).create(ApiInterface.class);
        Call<Repositories> call = api.getRepository(map);

        call.enqueue(new Callback<Repositories>() {
            @Override
            public void onResponse(Call<Repositories> call, Response<Repositories> response) {
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        Repositories repo = response.body();
                        ArrayList<Repository> repositoryArrayList = repo.getRepositoryArrayList();
                        if (repositoryArrayList != null && repositoryArrayList.size() > 0) {
                            repositoryAdapter = new RepositoryAdapter(getApplicationContext(), repositoryArrayList, MainActivity.this);
                            activityMainBinding.recyclerView.setAdapter(repositoryAdapter);
                        } else {
                            Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_LONG).show();
                            activityMainBinding.recyclerView.setVisibility(View.GONE);
                            activityMainBinding.emptyText.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Repositories> call, Throwable t) {
                System.out.println("failed url " + call.toString());
            }
        });
    }


    public void filteringOptionsDialog() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.filter_option_layout, null, false);
        dialog.setContentView(view);
        final Window window = dialog.getWindow();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        double d = (double) metrics.widthPixels;
        int screenWidth = (int) (d * 0.8d);
        window.setLayout(screenWidth, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.white);
        window.setGravity(Gravity.CENTER);
        dialog.show();

        final TextView startDate = (TextView) dialog.findViewById(R.id.start_date);
        final TextView endDate = (TextView) dialog.findViewById(R.id.end_date);
        TextView applyFilter = (TextView) dialog.findViewById(R.id.apply_filter);
        final TextView cancelFilter = (TextView) dialog.findViewById(R.id.clear_filer);

        startDate.setText("" + startDateString);
        endDate.setText("" + endDateString);

        applyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Map<String, String> map = new HashMap<>();
                String result = getFilterByDate();
                String query = getSearchString();
                String finalQuery = query + result;

                map.put("q", finalQuery);
                map.put("order", "desc");
                map.put("sort", "stars");

                getRepositories(map);
            }
        });
        cancelFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                clearFilters();
            }
        });


        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        monthOfYear++;
                        String mon = monthOfYear + "", day = "" + dayOfMonth;
                        if (mon.length() < 2)
                            mon = "0" + monthOfYear;
                        if (day.length() < 2)
                            day = "0" + dayOfMonth;

                        startDateString = year + "-" + mon + "-" + day;
                        startDate.setText(year + "-" + mon + "-" + day);
                    }
                }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        monthOfYear++;
                        String mon = monthOfYear + "", day = "" + dayOfMonth;
                        if (mon.length() < 2)
                            mon = "0" + monthOfYear;
                        if (day.length() < 2)
                            day = "0" + dayOfMonth;
                        endDateString = year + "-" + mon + "-" + day;

                        endDate.setText(year + "-" + mon + "-" + day);
                    }

                }, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
    }

    public String getFilterByDate() {
        String query = "";
        if (startDateString.length() > 0)
            query = " pushed:" + startDateString;

        if (endDateString.length() > 0) {
            query = query + ".." + endDateString;
        }
        return query;
    }

    public String getSearchString() {
        return activityMainBinding.search.getQuery().toString();
    }

    public void clearFilters() {
        startDateString = "";
        endDateString = "";
    }

    @Override
    public void OnRepoItemClick(String fullName) {
        Intent intent = new Intent(getApplicationContext(), RepoDetailsActivity.class);
        intent.putExtra("full_name", fullName);
        startActivity(intent);
    }
}
