package com.example.vinaytaskmapprr.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.vinaytaskmapprr.R;
import com.example.vinaytaskmapprr.activities.ContributorDetailsActivity;
import com.example.vinaytaskmapprr.model.Contributors;

import java.util.LinkedList;
import java.util.List;

public class ContributorAdapter extends RecyclerView.Adapter<ContributorAdapter.ContributorViewHolder> {

    private List<Contributors> contributors = new LinkedList<>();

    private Context context;

    public ContributorAdapter(Context context, List<Contributors> contributors) {
        this.contributors = contributors;
        this.context = context;
    }

    @Override
    public void onBindViewHolder(ContributorViewHolder holder, int position) {
        final Contributors contributor = contributors.get(position);
        if (contributor != null) {
            Glide.with(context)
                    .load(contributor.getAvatar_url())
                    .placeholder(R.mipmap.ic_launcher)
                    .centerCrop()
                    .into(holder.contributeImage);
            holder.contributeName.setText(contributor.getLogin());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = contributor.getRepos_url();
                    Intent intent = new Intent(context, ContributorDetailsActivity.class);
                    intent.putExtra("repos", url);
                    intent.putExtra("repo_url", contributor.getUrl());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }

    @NonNull
    @Override
    public ContributorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contributor_adapter, parent, false);
        return new ContributorViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return contributors.size();
    }

    class ContributorViewHolder extends RecyclerView.ViewHolder {

        TextView contributeName;
        ImageView contributeImage;

        ContributorViewHolder(View view) {
            super(view);
            contributeName = view.findViewById(R.id.contribute_name);
            contributeImage = view.findViewById(R.id.contribute_image);
        }
    }

}
