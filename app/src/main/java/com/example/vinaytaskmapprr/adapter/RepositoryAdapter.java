package com.example.vinaytaskmapprr.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.vinaytaskmapprr.R;
import com.example.vinaytaskmapprr.model.Repository;

import java.util.LinkedList;
import java.util.List;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryHolder> {

    private List<Repository> repositories = new LinkedList<>();
    private Context context;
    private RepoItemClick listener;

    public RepositoryAdapter(Context context, List<Repository> repoInfo, RepoItemClick listener) {
        repositories.addAll(repoInfo);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RepositoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_list_adapter, parent, false);
        return new RepositoryHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RepositoryHolder holder, int position) {
        final Repository repository = repositories.get(position);
        holder.userName.setText(repository.getName());
        holder.userFullName.setText(repository.getFull_name());
        holder.starsCount.setText(repository.getWatchers_count() + " Watcher");
        Glide.with(context)
                .load(repository.getOwner().getAvatar_url())
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.userImage);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnRepoItemClick(repository.getFull_name());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (repositories.size() > 10) {
            return 10;
        } else {
            return repositories.size();
        }
    }

    class RepositoryHolder extends RecyclerView.ViewHolder {

        TextView userName, userFullName, starsCount;

        ImageView userImage;
        CardView cardView;

        @SuppressLint("CutPasteId")
        RepositoryHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.card_view);
            userName = view.findViewById(R.id.name);
            userFullName = view.findViewById(R.id.full_name);
            starsCount = view.findViewById(R.id.watchers_count);
            userImage = view.findViewById(R.id.user_image);
        }
    }

    public interface RepoItemClick {
        void OnRepoItemClick(String fullName);
    }
}
